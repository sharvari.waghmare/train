﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Anonymous
{
    class Program
    {
        static void Main(string[] args)
        {
            MtButton button = new MtButton();
            button.Click += delegate ()
             {
                 Console.WriteLine("Empty Button Clicked!");
             };
            button.RaiseEvent();

        }
    }
    delegate void ClickHandler();

    class MtButton
    {
        public event ClickHandler Click;
        public void RaiseEvent()
        {
            Click();
        }
    }
}
