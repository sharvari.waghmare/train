﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Problem_1
{
    class Program
    {
        static void Main(string[] args)
        {
            User use = new User();
            Console.WriteLine("Enter the username");
            use.Username = Console.ReadLine();
            Console.WriteLine("Enter the password");
            use.Password = Console.ReadLine();

            Login l = new Login();
            l.LoginSuccessful += OnSuccess;
            l.LoginFailed += OnFail;
            var val=l.Authenticate(use);
            l.loginresult(val,use);
        }

        private static void OnFail(LoginEventArgs log)
        {
            Console.WriteLine("Hello {0},login failed with {1} password",log.Uname,log.Pass);
        }

        private static void OnSuccess(LoginEventArgs log)
        {
            Console.WriteLine("Hello {0},login Successful with {1} password", log.Uname, log.Pass);
        }
    }
}
