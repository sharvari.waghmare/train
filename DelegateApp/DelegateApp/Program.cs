﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DelegateApp
{
    delegate void Compute(int a);

    class Program
    {
        static void Main(string[] args)
        {
            // Compute c = new Compute(Square);
            //Compute c = Square;
            //var result=c.Invoke(13);
            //Console.WriteLine("Result is:{0}",result);
            Compute c = Cube;
            c += Square;
            c.Invoke(13);
            
            Console.ReadLine();
        }
        static void Square(int x)
        {
            Console.WriteLine("Square of {0} is:{1}",x,(x*x));
        }
        static void Cube(int y)
        {
            Console.WriteLine("Cube of {0} is {1}",y,(y*y*y));
        }
    }
}
