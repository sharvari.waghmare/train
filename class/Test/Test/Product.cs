﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;

namespace Test
{
    class Product
    {
        SqlConnection conn = new SqlConnection(@"Data Source=(localdb)\MSSQLLocalDB;Initial Catalog=SearchFlight;Integrated Security=True;Pooling=False");

        Cart c = new Cart();
        Helper h = new Helper();
        String input;
        int count;
        public void DeleteM()
        {
            Console.WriteLine("\n-------------------------------------------------------------------------------------------------------------------------------------------------------------");
            string comm7 = "delete from Cart";  //Delete Cart
            int a = h.Execute(comm7);

        }
        public int Count()
        {
            int cnt = 0;
            string comm = "select Count(Id) from Products";
            using (conn)
            {
                SqlDataReader reader = h.ReaderObj(comm);   //Count Total products from Products table
                while (reader.Read())
                {
                    cnt = Convert.ToInt32(reader[0]);

                }
                reader.Close();
                h.ConnClose();
            }

            return cnt;
        }
        public void Display()
        {
            string disp = "select * from Products";
            int j = 0;
            SqlDataReader reader = h.ReaderObj(disp);   //Display Products details
            while (reader.Read())
            {
                j++;
                Console.WriteLine(j + string.Format(".ID: {0,-5}Name: {1,-10}\tDescription: {2,-20}Category: {3,-15}Price: {4}", reader["Id"], reader["Name"], reader["Description"], reader["Category"], reader["Price"]));
                Console.WriteLine(); //Displaying Product
            }

            h.ConnClose();
            reader.Close();
           
        }
        public void CategoryDisp(string query)
        {
            string comm6 = $"select * from Products where Category='{query}'";
            SqlDataReader reader2 = h.ReaderObj(comm6);
            while (reader2.Read())
            {
                Console.WriteLine(string.Format("ID: {0,-5}Name: {1,-10}\tDescription: {2,-20}Category: {3,-15}Price: {4}", reader2["Id"], reader2["Name"], reader2["Description"], reader2["Category"], reader2["Price"]));
                Console.WriteLine();   //Displaying Product Categorywise
            }

            reader2.Close();
            h.ConnClose();

        }
        public void CatDistinct(List<string> category, List<string> nameA, List<int> cart)
        {
            string comm5 = "select distinct(Category) from Products";
            SqlDataReader reader = h.ReaderObj(comm5);
            int i = 0;
            while (reader.Read())
            {
                i++;
                Console.WriteLine(string.Format("{0} {1}", i, reader["Category"]));//Categories in Products
                category.Add(Convert.ToString(reader["Category"]));
                Console.WriteLine();
            }
            reader.Close();
            h.ConnClose();
            Console.WriteLine("\nEnter the Category:");
            int choice2 = Convert.ToInt32(Console.ReadLine());
            Console.Clear();
            Console.WriteLine("\n1.Display Category Products\n2.Add Product");
            int choice6 = Convert.ToInt32(Console.ReadLine());
            if (choice6 <= 2)
            {
                switch (choice6)
                {
                    case 1:
                        Console.Clear();
                        Console.WriteLine("\n-------------------------------------------------------------------------------------------------------------------------------------------------------------");
                        CategoryDisp(category[choice2 - 1]);//Display category products
                        break;
                    case 2:
                        Console.Clear();
                        Console.WriteLine("\n-------------------------------------------------------------------------------------------------------------------------------------------------------------");
                        count = Count();
                        do
                        {
                            CategoryDisp(category[choice2 - 1]);
                            Console.WriteLine("\nEnter the ID to be added to cart:");//Add Product to category

                            int choice7 = Convert.ToInt32(Console.ReadLine());
                            if (choice7 <= count)
                            {
                                Console.WriteLine("\nEnter the Quantity:");
                                int qty = Convert.ToInt32(Console.ReadLine());
                                if (cart.Contains(choice7))
                                {
                                    Console.WriteLine("\nProduct already present!!");

                                }
                                else
                                {
                                    int v1 = c.Insert(qty, choice7);
                                    if (v1 == 1)
                                    {
                                        cart.Add(choice7);
                                        Console.WriteLine("\nProduct Added to Cart!!");  //Product added to cart
                                    }
                                }
                            }
                            else
                            {
                                Console.WriteLine("\nEnter a Valid Id...!!!");
                            }
                            Console.WriteLine("\nAdd more Products? Y/N"); //Continue if more products to be added
                            input = Console.ReadLine();
                        } while (input == "Y");
                        break;


                    default:
                        break;
                }


            }
        }

    }
}
