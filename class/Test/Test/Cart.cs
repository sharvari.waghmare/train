﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;

namespace Test
{
    class Cart
    {
        SqlConnection conn = new SqlConnection(@"Data Source=(localdb)\MSSQLLocalDB;Initial Catalog=SearchFlight;Integrated Security=True;Pooling=False");
        List<string> namea = new List<string>();
        Helper h = new Helper();

        public int Insert(int qty, int choice3)
        {
            int result;
            string insertCommand = "insert into Cart (Name,Category,Price,Quantity) select Name,Category,Price," + qty + " from Products where Id=" + choice3;
            string sub = "Update Cart Set Subtotal=Price*Quantity";
            result = h.Execute(insertCommand);  //add product to the Cart
            var result1 = h.Execute(sub);
            return result;
        }
        public void Summary()
        {
            Console.WriteLine("\n-------------------------------------------------------------------------------------------------------------------------------------------------------------");
            string comm4 = "select sum(Quantity),sum(Subtotal) from Cart";
            using (conn)
            {
                SqlDataReader reader = h.ReaderObj(comm4);
                while (reader.Read())
                {
                    Console.WriteLine(string.Format("Total Items: {0,-5}Cart Price: {1,-10}", reader[0], reader[1]));//Display summary of Cart
                    Console.WriteLine();
                }
                reader.Close();
                h.ConnClose();
            }
            Console.WriteLine("\n-----------------------------------------------------------------------------------------------------------------------------------------------------------------");
        }
        public void Display()
        {
            Console.WriteLine("\n-----------------------------------------------------------------------------------------------------------------------------------------------------------------");
            string comm3 = "select * from Cart";
            using (conn)
            {
                int j = 0;
                SqlDataReader reader = h.ReaderObj(comm3);//Displaying cart Details
                while (reader.Read())
                {
                    j++;
                    Console.WriteLine(j + string.Format(".Quantity: {0,-5}Name: {1,-10}\tPrice: {2}\tSubtotal: {3}", reader["Quantity"], reader["Name"], reader["Price"], reader["Subtotal"]));
                    Console.WriteLine();

                }
                reader.Close();
                h.ConnClose();
            }
            Console.WriteLine("\n-----------------------------------------------------------------------------------------------------------------------------------------------------------------");
        }
        public int Query(string command)
        {
            int result2;
            result2 = h.Execute(command);//Helper function
            return result2;
        }
        public void Update()
        {
            Console.WriteLine("\n-------------------------------------------------------------------------------------------------------------------------------------------------------------");
            string comm2 = "select * from Cart";
            using (conn)
            {
                int j = 0;
                SqlDataReader reader = h.ReaderObj(comm2);//Displaying cart Details
                while (reader.Read())
                {
                    j++;
                    Console.WriteLine(j + string.Format(".Quantity: {0,-5}Name: {1,-10}\tPrice: {2}\tSubtotal: {3}", reader["Quantity"], reader["Name"], reader["Price"], reader["Subtotal"]));
                    Console.WriteLine();
                    namea.Add(Convert.ToString(reader["Name"]));
                }
                reader.Close();
                h.ConnClose();
                Console.WriteLine("\nEnter the No. of the Product for Quantity to be changed");//Update the quantity of a existing product
                int choice4 = Convert.ToInt32(Console.ReadLine());
                Console.WriteLine("\nEnter the Quantity:");
                int qtychange = Convert.ToInt32(Console.ReadLine());
                string chnge = namea[choice4 - 1];
                string quantity = $"Update Cart set Quantity= {qtychange} where Name='{chnge}'";
                int arg1 = Query(quantity);
            }
        }
        public void Deleteq()
        {
            Console.WriteLine("\n-------------------------------------------------------------------------------------------------------------------------------------------------------------");
            Display();
            Console.WriteLine("\nEnter the product to be removed:");
            int choice5 = Convert.ToInt32(Console.ReadLine());

            try
            {
                string del = namea[choice5 - 1];
                string deletep = $"delete from Cart where Cart.Name='{del}'";
                int arg = Query(deletep);//Remove Product from Cart
                Console.WriteLine("\n Product Deleted!!!");
            }
            catch (System.ArgumentOutOfRangeException)
            {

                Console.WriteLine("\nEnter valid Product!!!");
            }

        }
    }
}
