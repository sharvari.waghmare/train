﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;

namespace Test
{
    class Helper
    {
        SqlConnection conn = new SqlConnection(@"Data Source=(localdb)\MSSQLLocalDB;Initial Catalog=SearchFlight;Integrated Security=True;Pooling=False");
        SqlDataReader reader1;
        public int Execute(string comm7)
        {
            
                conn.Open();
                SqlCommand command = new SqlCommand();
                command.CommandText = comm7;//Helper function for DML
                command.CommandType = System.Data.CommandType.Text;
                command.Connection = conn;
                var result = command.ExecuteNonQuery();

                conn.Close();
                return result;
        }
        public SqlDataReader ReaderObj(string comm)
        {
           
                conn.Open();
                SqlCommand command = new SqlCommand();
                command.CommandText = comm;
                command.CommandType = System.Data.CommandType.Text;
                command.Connection = conn;//Helper function for DDL
                reader1 = command.ExecuteReader();
                return reader1;
            
        }
        public void ConnClose()
        {
            reader1.Close();
            conn.Close();
        }
    }
}
