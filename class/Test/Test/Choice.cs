﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test
{
    class Choice
    {
        Product p = new Product();
        Cart c = new Cart();
        int count;
        string input;
        public void ProductDetails(List<string> category, List<string> nameA, List<int> cart)
        {
            Console.WriteLine("\n-----------------------------------------------------------------------------------------------------------------------------------------------------------------");
            Console.WriteLine("1.All Product Details\n2.Category wise Details");
            int choice1 = Convert.ToInt32(Console.ReadLine());
            if (choice1 == 1)
            {
                Console.Clear();//All Product details
                Console.WriteLine("\n-------------------------------------------------------------------------------------------------------------------------------------------------------------");

                p.Display();

                Console.WriteLine("\n-----------------------------------------------------------------------------------------------------------------------------------------------------------------");
            }
            else
            {
                Console.Clear();//Category wise details
                Console.WriteLine("\n-------------------------------------------------------------------------------------------------------------------------------------------------------------");

                Console.WriteLine("Categories are:");
                p.CatDistinct(category, nameA, cart);

                Console.WriteLine("\n-----------------------------------------------------------------------------------------------------------------------------------------------------------------");
            }
        }
        public void AddToCart(List<string> category, List<string> nameA, List<int> cart)
        {
            count = p.Count();
            do
            {
                Console.Clear();
                Console.WriteLine("\n-------------------------------------------------------------------------------------------------------------------------------------------------------------");

                p.Display();
                Console.WriteLine("\nEnter the ID to be added to cart:");

                int choice3 = Convert.ToInt32(Console.ReadLine());
                if (choice3 <= count)
                {
                    Console.WriteLine("\nEnter the Quantity:");
                    int qty = Convert.ToInt32(Console.ReadLine());
                    if (cart.Contains(choice3))
                    {
                        Console.WriteLine("\nProduct already present!!");

                    }
                    else
                    {
                        int v = c.Insert(qty, choice3);
                        if (v == 1)
                        {
                            cart.Add(choice3);
                            Console.WriteLine("\nProduct Added to Cart!!");      //Add product to Cart
                        }
                    }
                }
                else
                {
                    Console.WriteLine("\nEnter a Valid Id...!!!");
                }
                Console.WriteLine("\nAdd more Products? Y/N");
                input = Console.ReadLine();
            } while (input == "Y");
        }

    }
}
