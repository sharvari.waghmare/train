﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace Test
{
    class Program
    {

        static void Main(string[] args)
        {
          
            Product p = new Product();
            Cart c = new Cart();
            Choice obj = new Choice();
            List<string> category = new List<string>();
            List<string> nameA = new List<string>();
            List<int> cart = new List<int>();
            bool val = true;
            p.DeleteM();
            int count = p.Count();
            while (val)
            {
                try
                {
                    Console.Clear();
                    if (cart.Count == 0)
                    {
                        Console.WriteLine("\n-------------SHOP--------------");
                        Console.WriteLine("\n1.List Of Products\n2.Add Product to Cart\n0.Exit");   //if cart is empty
                        Console.WriteLine("\n-------------------------------");

                    }
                    else
                    {
                        Console.WriteLine("\n-------------SHOP--------------");

                        Console.WriteLine("\n1.List Of Products\n2.Add Product to Cart\n3.Cart Details\n4.Add quantity of existing Product\n5.Remove Product from Cart\n6.Summary of Cart\n0.Exit");
                        Console.WriteLine("\n-------------------------------");

                    }
                    int choice = Convert.ToInt32(Console.ReadLine());
                    if ((choice >= 0) && (choice < 8))
                    {
                        switch (choice)
                        {
                            case 1:
                                Console.Clear();
                                obj.ProductDetails(category, nameA, cart);//Display Product details
                                break;
                            case 2:
                                obj.AddToCart(category, nameA, cart);//Add Product to cart
                                break;
                            case 3:
                                Console.Clear();
                                c.Display();//Display Cart details 
                                break;
                            case 4:
                                Console.Clear();
                                c.Update();//Update Quantity
                                Console.WriteLine("\n Quantity Updated!!");
                                break;
                            case 5:
                                Console.Clear();
                                c.Deleteq();//Remove from Cart
                                break;

                            case 6:
                                Console.Clear();
                                c.Summary();//Summary of Cart
                                break;
                            case 0:
                                Console.Clear();
                                DialogResult res = MessageBox.Show("Are you sure you want to Exit??", "Confirmation", MessageBoxButtons.OKCancel, MessageBoxIcon.Information);
                                if (res == DialogResult.OK)
                                {
                                    p.DeleteM();
                                    val = false;
                                }
                                if (res == DialogResult.Cancel)
                                {
                                    val = true;
                                }
                                //Delete Cart
                                break;
                            default:
                                Console.WriteLine("\n-------------------------------------------------------------------------------------------------------------------------------------------------------------");
                                break;
                        }

                    }
                    else
                    {
                        Console.WriteLine("\nEnter a Valid Choice.....");
                    }
                    Console.WriteLine("\nContinue Shopping? Y/N");
                    if ((Console.ReadLine() == "Y") || (Console.ReadLine() == "y"))
                    {
                        val = true;
                    }
                    else
                    {
                        val = false;
                    }

                }
                catch (System.FormatException)
                {

                    Console.WriteLine("\nEnter Correct input Data....");
                    Console.ReadLine();
                }

            }
            // Console.ReadLine();

        }
    }




}
