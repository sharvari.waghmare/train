﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App1
{
    class Program
    {
        static void Main(string[] args)
        {

            //2Console.WriteLine("Printers available:");
            //Console.WriteLine("1.Laser");
            //Console.WriteLine("2.Dot Matrix");
            //Console.WriteLine("Enter your choice");
            //int choice = Convert.ToInt32(Console.ReadLine());
            //IPrinter printer;

            //switch (choice)
            //{
            //    case 1:
            //        printer = new LaserPrinter();
            //        printer.Print();
            //        break;
            //    case 2:
            //        printer = new DotMatrix();
            //        printer.Print();
            //        break;
            //    default:
            //        Console.WriteLine("PLease enter either 1 or 2 as your choice....");
            //        break;
            //}

            //1Printer printer = new LaserPrinter();
            //printer.Print();
            //printer = new DotMatrix();
            //printer.Print();
            //{ 
            //int x;
            //Console.WriteLine("In main()");
            ////Console.WriteLine("x before calling  change() is {0}", x);
            //Change(out x);
            //Console.WriteLine("In main()");
            //Console.WriteLine("x after calling  change() is {0}", x); }
            DrawPolygon(10,12);
            DrawPolygon(10,11,12,13);
            Console.ReadLine();



        }
        static void DrawPolygon(params int[] points)
        {
            Console.WriteLine("A polygon of {0} sides.....",points.Length);
        }
        static void Change(out int x)
        {
            Console.WriteLine("In change()");
           // Console.WriteLine("x before change is {0}",x);
            x = 123;
            Console.WriteLine("In change()");
            Console.WriteLine("x after change is {0}", x);
        }
    }
    public interface IPrinter
    {
       void Print();
        
        
    }
    public class LaserPrinter : IPrinter
    {
        public void Print()
        {
            Console.WriteLine("This is laser print...");
        }
    }
    public class DotMatrix : IPrinter
    {
        public void Print()
        {
            Console.WriteLine("This is dot matrix print...");
        }
    }
    public sealed class Product
    {

    }




}
