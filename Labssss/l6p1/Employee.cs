﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace l6p1
{
    class Employee
    {
        private int eId;
        private string ename, des,doj1;
        private static int count;
        private double bsalary, HRA, DA, PF, GS, NS, Medical, PT;
        public Employee(string name, string designation, double sal, double Med, int id,string doj)
        {
            count++;
            eId = id;
            ename = name;
            des = designation;
            bsalary = sal;
            Medical = Med;
            doj1 = doj;

        }
        public void calc()
        {
            HRA = (8 * bsalary) / 100;
            PF = (12 * bsalary) / 100;
            GS = bsalary + HRA + Medical;
        }
        public override string ToString()
        {
            calc();
            //NS = GS - (PT+PF);
            return "Employee Name-> " + ename + " Employee Id-> " + eId + "Gross Salary-> " + GS;
            //Console.WriteLine("-----EMPLOYEE DETAILS-------");
            //Console.WriteLine("EName:" + ename);
            //Console.WriteLine("Designation:" + des);
            //Console.WriteLine("EID:" + eId);
            //Console.WriteLine("HRA:" + HRA);
            //Console.WriteLine("PF:" + PF);
            //Console.WriteLine("GS:" + GS);
            //Console.WriteLine("Employee Count:" + count);
            //Console.ReadLine();
        }


    }
}
