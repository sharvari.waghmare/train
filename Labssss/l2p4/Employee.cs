﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace l2p4
{
    class Employee
    {
        int eId;
        string ename, des;
        static int count;
        double bsalary, HRA, DA, PF, GS, NS, Medical, PT;
        public Employee(string name, string designation, double sal, double Med, int id)
        {
            count++;
            eId = id;
            ename = name;
            des = designation;
            bsalary = sal;
            Medical = Med;

        }
        public void calc()
        {
            HRA = (8 * bsalary) / 100;
            PF = (12 * bsalary) / 100;
            GS = bsalary + HRA + Medical;
        }
        public void display()
        {
            calc();
            //NS = GS - (PT+PF);
            Console.WriteLine("-----EMPLOYEE DETAILS-------");
            Console.WriteLine("EName:" + ename);
            Console.WriteLine("Designation:" + des);
            Console.WriteLine("EID:" + eId);
            Console.WriteLine("HRA:" + HRA);
            Console.WriteLine("PF:" + PF);
            Console.WriteLine("GS:" + GS);
            Console.WriteLine("Employee Count:" + count);
            Console.ReadLine();
        }
        

    }
}
