﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace l2p4
{
    class Department
    {
        int Id;
        string dname, location;

        public Department(string name, string loc, int id)
        {

            Id = id;
            dname = name;
            location = loc;


        }
        public void display()
        {
            Console.WriteLine("-----DEPARTMENT DETAILS-------");
            Console.WriteLine("Department Id:" + Id);
            Console.WriteLine("Department name:" + dname);
            Console.WriteLine("Department Location:" + location);

        }

    }
}
